extends HBoxContainer

var currentIsColor: bool = true
var extended : bool = false

var settings : TBSettings

var colorModeToggle   : CheckButton
var bgColourPicker    : ColorPickerButton

var plainColourPicker : ColorPickerButton

var hueFactorSlider   : HSlider
var hueFactorLabel    : Label
var hueOffsetSlider   : HSlider
var hueOffsetLabel    : Label

var valueFactorSlider : HSlider
var valueFactorLabel  : Label
var valueRangeSlider  : HSlider
var valueRangeLabel   : Label
var valueClampSlider  : HSlider
var valueClampLabel   : Label

var saturationSlider  : HSlider
var satLabel          : Label

var frsqSlider        : HSlider
var frsqLabel         : Label
var mrsqSlider        : HSlider
var mrsqLabel         : Label
var foldlimSlider     : HSlider
var foldlimLabel      : Label
var scaleSlider       : HSlider
var scaleLabel        : Label
var iterationsSlider  : HSlider
var iterLabel         : Label

var maxstepsInput     : LineEdit
var colldistInput     : LineEdit

var plainColorList    : VBoxContainer
var rainbowColorList  : VBoxContainer

var maxStepsText : String
var collDistText : String

var exportWidthInput  : LineEdit
var exportHeightInput : LineEdit
var exportKeepAspect  : bool
var lockIndicator     : TextureRect

var exportWidth  : int
var exportHeight : int
var exportAspect : float

var lockedImg   : ImageTexture
var unlockedImg : ImageTexture


signal settings_updated()
signal screenshot_requested(size : Vector2i)


# Called when the node enters the scene tree for the first time.
func _ready():
    _assign_aliases()
    _changeActiveMenu(true)

    # Initial state for color panel
    colorModeToggle.set_pressed_no_signal(true)
    plainColorList.visible = false;
    rainbowColorList.visible = true;
    exportKeepAspect = true

    # Initial state for screenshot parameters
    lockedImg = ImageTexture.create_from_image(ResourceLoader.load("res://assets/locked.png"))
    unlockedImg = ImageTexture.create_from_image(ResourceLoader.load("res://assets/unlocked.png"))
    lockIndicator.set_texture(lockedImg)

 # Default to standard 4K 16:9
    exportWidth = 3840
    exportHeight = 2160
    exportAspect = float(exportWidth)/float(exportHeight)

    exportWidthInput.text  = str(exportWidth)
    exportHeightInput.text = str(exportHeight)


    retract()

func load_settings(newsettings: TBSettings) -> void:
    settings = newsettings
    bgColourPicker.set_pick_color(settings.bg_colour)

    plainColourPicker.set_pick_color(settings.plain_colour)

    hueFactorSlider.set_value_no_signal(settings.hue_factor)
    hueFactorLabel.text = str(settings.hue_factor)
    hueOffsetSlider.set_value_no_signal(settings.hue_offset)
    hueOffsetLabel.text = str(settings.hue_offset)

    valueFactorSlider.set_value_no_signal(settings.value_factor)
    valueFactorLabel.text = str(settings.value_factor)
    valueRangeSlider.set_value_no_signal(settings.value_range)
    valueRangeLabel.text = str(settings.value_range)
    valueClampSlider.set_value_no_signal(settings.value_clamp)
    valueClampLabel.text = str(settings.value_clamp)

    saturationSlider.set_value_no_signal(settings.sat_value)
    satLabel.text = str(settings.sat_value)

    frsqSlider.set_value_no_signal(settings.fixed_radius_squared)
    frsqLabel.text = str(settings.fixed_radius_squared)
    mrsqSlider.set_value_no_signal(settings.minimum_radius_squared)
    mrsqLabel.text = str(settings.minimum_radius_squared)
    foldlimSlider.set_value_no_signal(settings.folding_limit)
    foldlimLabel.text = str(settings.folding_limit)
    scaleSlider.set_value_no_signal(settings.box_scale)
    scaleLabel.text = str(settings.box_scale)
    iterationsSlider.set_value_no_signal(settings.box_iterations)
    iterLabel.text = str(settings.box_iterations)

    maxstepsInput.text = str(settings.max_ray_steps)
    maxStepsText = maxstepsInput.text

    colldistInput.text = str(settings.collision_min_dist)
    collDistText = colldistInput.text


func extend() -> void:
    $MenuPanel.custom_minimum_size.x = 400
    extended = true;

func retract() -> void:
    $MenuPanel.custom_minimum_size.x = 0
    extended = false;

func _changeActiveMenu(to_color : bool) -> void:
    if(to_color):
        currentIsColor = true;
        $MenuPanel/ColorPanel.size_flags_horizontal = SIZE_EXPAND_FILL
        $MenuPanel/ParamPanel.size_flags_horizontal = SIZE_FILL
    else:
        currentIsColor = false;
        $MenuPanel/ColorPanel.size_flags_horizontal = SIZE_FILL
        $MenuPanel/ParamPanel.size_flags_horizontal = SIZE_EXPAND_FILL

func _on_color_btn_pressed():
    if(extended && currentIsColor):
        retract()
    else:
        extend()
        _changeActiveMenu(true)

func _on_param_btn_pressed():
    if(extended && !currentIsColor):
        retract()
    else:
        extend()
        _changeActiveMenu(false)


func _on_colormode_toggled(button_pressed):

    settings.plain_mode      = !button_pressed
    plainColorList.visible   = !button_pressed
    rainbowColorList.visible = button_pressed
    emit_signal("settings_updated")

func _assign_aliases() -> void:
    colorModeToggle = $MenuPanel/ColorPanel/VBoxContainer/row_colormode/Toggle as CheckButton
    bgColourPicker  = $MenuPanel/ColorPanel/VBoxContainer/row_bg/Picker as ColorPickerButton

    plainColourPicker  = $MenuPanel/ColorPanel/VBoxContainer/PlainList/row_plain/Picker as ColorPickerButton

    hueFactorSlider = $MenuPanel/ColorPanel/VBoxContainer/RainbowList/row_huefactor/Slider as HSlider
    hueFactorLabel = $MenuPanel/ColorPanel/VBoxContainer/RainbowList/row_huefactor/ValueLabel as Label
    hueOffsetSlider = $MenuPanel/ColorPanel/VBoxContainer/RainbowList/row_hueoffset/Slider as HSlider
    hueOffsetLabel = $MenuPanel/ColorPanel/VBoxContainer/RainbowList/row_hueoffset/ValueLabel as Label

    valueFactorSlider = $MenuPanel/ColorPanel/VBoxContainer/row_valfactor/Slider as HSlider
    valueFactorLabel = $MenuPanel/ColorPanel/VBoxContainer/row_valfactor/ValueLabel as Label
    valueRangeSlider = $MenuPanel/ColorPanel/VBoxContainer/row_valrange/Slider as HSlider
    valueRangeLabel = $MenuPanel/ColorPanel/VBoxContainer/row_valrange/ValueLabel as Label
    valueClampSlider = $MenuPanel/ColorPanel/VBoxContainer/row_valclamp/Slider as HSlider
    valueClampLabel = $MenuPanel/ColorPanel/VBoxContainer/row_valclamp/ValueLabel as Label

    saturationSlider = $MenuPanel/ColorPanel/VBoxContainer/RainbowList/row_sat/Slider as HSlider
    satLabel = $MenuPanel/ColorPanel/VBoxContainer/RainbowList/row_sat/ValueLabel as Label

    frsqSlider = $MenuPanel/ParamPanel/ParamList/row_frsq/Slider as HSlider
    frsqLabel = $MenuPanel/ParamPanel/ParamList/row_frsq/ValueLabel as Label
    mrsqSlider = $MenuPanel/ParamPanel/ParamList/row_mrsq/Slider as HSlider
    mrsqLabel = $MenuPanel/ParamPanel/ParamList/row_mrsq/ValueLabel as Label
    foldlimSlider = $MenuPanel/ParamPanel/ParamList/row_foldlim/Slider as HSlider
    foldlimLabel = $MenuPanel/ParamPanel/ParamList/row_foldlim/ValueLabel as Label
    scaleSlider = $MenuPanel/ParamPanel/ParamList/row_scale/Slider as HSlider
    scaleLabel = $MenuPanel/ParamPanel/ParamList/row_scale/ValueLabel as Label
    iterationsSlider = $MenuPanel/ParamPanel/ParamList/row_iter/Slider as HSlider
    iterLabel = $MenuPanel/ParamPanel/ParamList/row_iter/ValueLabel as Label

    maxstepsInput = $MenuPanel/ParamPanel/ParamList/row_maxsteps/Input as LineEdit
    colldistInput = $MenuPanel/ParamPanel/ParamList/row_colldist/Input as LineEdit

    plainColorList = $MenuPanel/ColorPanel/VBoxContainer/PlainList as VBoxContainer
    rainbowColorList = $MenuPanel/ColorPanel/VBoxContainer/RainbowList as VBoxContainer

    exportWidthInput = $MenuPanel/ParamPanel/ParamList/row_imgsize/InputW as LineEdit
    exportHeightInput = $MenuPanel/ParamPanel/ParamList/row_imgsize/InputH as LineEdit
    lockIndicator = $MenuPanel/ParamPanel/ParamList/row_imgsize/LockIndicator as TextureRect

func _on_plain_color_changed(color):
    settings.plain_colour = color
    emit_signal("settings_updated")

func _on_hue_factor_changed(value):
    hueFactorLabel.text = str(value)
    settings.hue_factor = value
    emit_signal("settings_updated")

func _on_hue_offset_changed(value):
    hueOffsetLabel.text = str(value)
    settings.hue_offset = value
    emit_signal("settings_updated")

func _on_value_factor_changed(value):
    valueFactorLabel.text = str(value)
    settings.value_factor = value
    emit_signal("settings_updated")


func _on_value_range_changed(value):
    valueRangeLabel.text = str(value)
    settings.value_range = value
    emit_signal("settings_updated")


func _on_value_clamp_changed(value):
    valueClampLabel.text = str(value)
    settings.value_clamp = value
    emit_signal("settings_updated")


func _on_saturation_changed(value):
    satLabel.text = str(value)
    settings.sat_value = value
    emit_signal("settings_updated")


func _on_fixed_radius_squared_changed(value):
    frsqLabel.text = str(value)
    settings.fixed_radius_squared = value
    emit_signal("settings_updated")


func _on_minimum_radius_squaed_changed(value):
    mrsqLabel.text = str(value)
    settings.minimum_radius_squared = value
    emit_signal("settings_updated")


func _on_folding_limit_changed(value):
    foldlimLabel.text = str(value)
    settings.folding_limit = value
    emit_signal("settings_updated")


func _on_box_scale_changed(value):
    scaleLabel.text = str(value)
    settings.box_scale = value
    emit_signal("settings_updated")


func _on_iterations_changed(value):
    iterLabel.text = str(value)
    settings.box_iterations = value
    emit_signal("settings_updated")


func _on_max_steps_submitted(new_text):
    if(new_text.is_valid_int()):
        var val : int = int(new_text)
        settings.max_ray_steps = val
        maxStepsText = new_text
        emit_signal("settings_updated")
    else:
        maxstepsInput.text = maxStepsText

func _on_maxsteps_focus_exited():
    _on_max_steps_submitted(maxstepsInput.text)

func _on_collision_distance_submitted(new_text):
    if(new_text.is_valid_float()):
        var val : float = float(new_text)
        settings.collision_min_dist = val
        collDistText = new_text
        emit_signal("settings_updated")
    else:
        colldistInput.text = collDistText

func _on_colldist_focus_exited():
    _on_collision_distance_submitted(colldistInput.text)


func _on_capture_btn_pressed():
    emit_signal("screenshot_requested", Vector2i(exportWidth,exportHeight))


func _on_keep_aspect_toggled(toggled_on):
    exportKeepAspect = toggled_on
    lockIndicator.set_texture(lockedImg if toggled_on else unlockedImg)
    if(toggled_on):
        exportHeight = int(float(exportWidth)/float(exportAspect))
        exportHeightInput.text = str(exportHeight)

func _on_screenshot_width_submitted(new_text : String):
    if(new_text.is_valid_int()):
        var val : int = int(new_text)
        if(val > 0):
            exportWidth = val
            if(exportKeepAspect):
                exportHeight = int(float(exportWidth)/float(exportAspect))
                exportHeightInput.text = str(exportHeight)
            else:
                exportAspect = float(exportWidth)/float(exportHeight)
            return
    # Not valid, so change it back
    exportWidthInput.text = str(exportWidth)


func _on_screenshot_height_submitted(new_text : String):
    if(new_text.is_valid_int()):
        var val : int = int(new_text)
        if(val > 0):
            exportHeight = val
            if(exportKeepAspect):
                exportWidth = int(exportHeight * exportAspect)
                exportWidthInput.text = str(exportWidth)
            else:
                exportAspect = float(exportWidth)/float(exportHeight)
            return
    # Not valid, so change it back
    exportHeightInput.text = str(exportHeight)
