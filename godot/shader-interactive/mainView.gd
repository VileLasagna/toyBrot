extends Control

@export var settings : TBSettings
var file_dialog : FileDialog
var img_to_save : Image
var dialog_is_img : bool = true

# Don't static type this because JavaScriptObject is not present on non HTML5 exports
var _on_data_loaded_callback = null

signal settings_loaded()

# Called when the node enters the scene tree for the first time.
func _ready():
    if settings == null :
        settings = TBSettings.new()
    _update_env()
    update_shader()
    $SideBar.load_settings(settings)
    $FractalContainer/FractalViewport.size = size
    if (OS.get_name() == "Web"):
        _on_data_loaded_callback = JavaScriptBridge.create_callback(_on_settings_uploaded)
        # Retrieve the 'gd_callbacks' object
        var gdcallbacks: JavaScriptObject = JavaScriptBridge.get_interface("gd_callbacks")
        # Assign the callbacks
        gdcallbacks.dataLoaded = _on_data_loaded_callback

    else:
        file_dialog = FileDialog.new()
        add_child(file_dialog)
        file_dialog.dialog_hide_on_ok = true
        file_dialog.set_access(FileDialog.ACCESS_FILESYSTEM)
        file_dialog.size = Vector2(500, 400)
        file_dialog.connect("file_selected", _on_file_selected)
        file_dialog.set_initial_position(Window.WINDOW_INITIAL_POSITION_CENTER_MAIN_WINDOW_SCREEN)

func _process(_delta):
    $FPSLabel.set_text("FPS: " + str(Engine.get_frames_per_second()))

func _save_image():
    if(OS.get_name() == "Web"):
        img_to_save.save_png("res://screenshot.png")
        var temp_file = FileAccess.get_file_as_bytes("res://screenshot.png")
        JavaScriptBridge.download_buffer(temp_file, "toyBrotInteractive.png", "img/png")
    else:
        file_dialog.set_filters(["*.png; PNG images"])
        file_dialog.set_file_mode(FileDialog.FILE_MODE_SAVE_FILE)
        file_dialog.set_current_dir(OS.get_system_dir(OS.SYSTEM_DIR_PICTURES))
        dialog_is_img = true
        file_dialog.popup()

func _save_settings():
    if(OS.get_name() == "Web"):
        ResourceSaver.save(settings, "res://settings.tres")
        var temp_file = FileAccess.get_file_as_bytes("res://settings.tres")
        JavaScriptBridge.download_buffer(temp_file, "toyBrotInteractive.tres", "text/plain;charset=UTF-8")
    else:
        file_dialog.set_filters(["*.tres; Godot Resource files"])
        file_dialog.set_file_mode(FileDialog.FILE_MODE_SAVE_FILE)
        file_dialog.set_current_dir(OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS))
        dialog_is_img = false
        file_dialog.popup()

func _load_settings():
    if(OS.get_name() == "Web"):
        if (OS.has_feature("web")):
            JavaScriptBridge.eval("loadData()")
        else:
            print("ERROR: No Javascript")
    else:
        file_dialog.set_filters(["*.tres; Godot Resource files"])
        file_dialog.set_file_mode(FileDialog.FILE_MODE_OPEN_FILE)
        file_dialog.set_current_dir(OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS))
        dialog_is_img = false
        file_dialog.popup()

func _on_settings_uploaded(data: Array):
    #callback function to handle loading in Web
    if (data.size() == 0):
        return
    var temp_file = FileAccess.open("res://new_settings.tres",FileAccess.WRITE)
    temp_file.store_string(data[0])
    temp_file.close()
    settings = ResourceLoader.load("res://new_settings.tres", "TBSettings")
    _update_env()
    update_shader()
    $SideBar.load_settings(settings)

func _on_file_selected(path):
    if(dialog_is_img):
        img_to_save.save_png(path)
    else:
        if(file_dialog.file_mode == FileDialog.FILE_MODE_SAVE_FILE):
            ResourceSaver.save(settings, path)
        else:
            settings = ResourceLoader.load(path, "TBSettings")
            _update_env()
            update_shader()
            $SideBar.load_settings(settings)

func _update_env() -> void:
    $FractalContainer/FractalViewport/Camera3D.set_global_position(settings.camera_position)
    $FractalContainer/FractalViewport/Camera3D.set_rotation_degrees(settings.camera_rot_deg)
    $FractalContainer/FractalViewport/Camera3D.set_fov(settings.camera_fov)
    $FractalContainer/FractalViewport/Camera3D.set_near(settings.camera_near)
    set_background(settings.bg_colour)

func _store_camera() -> void:
    settings.camera_position = $FractalContainer/FractalViewport/Camera3D.get_global_position()
    settings.camera_rot_deg  = $FractalContainer/FractalViewport/Camera3D.get_rotation_degrees()
    settings.camera_fov      = $FractalContainer/FractalViewport/Camera3D.get_fov()
    settings.camera_near     = $FractalContainer/FractalViewport/Camera3D.get_near()

func update_shader() -> void:

    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("plainMode", settings.plain_mode)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("plainColour", settings.plain_colour)

    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("hueFactor", settings.hue_factor)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("hueOffset", settings.hue_offset)

    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("valueFactor", settings.value_factor)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("valueRange", settings.value_range)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("valueClamp", settings.value_clamp)

    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("satValue", settings.sat_value)

    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("maxRaySteps", settings.max_ray_steps)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("collisionMinDist", settings.collision_min_dist)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("escapeDiscardDist", 30.0)

    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("fixedRadiusSq", settings.fixed_radius_squared)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("minRadiusSq", settings.minimum_radius_squared)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("foldingLimit", settings.folding_limit)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("boxScale", settings.box_scale)
    $FractalContainer/FractalViewport/FractalBox.mesh.material.set_shader_parameter("boxIterations", settings.box_iterations)

func set_background(new_color : Color) -> void:
    $FractalContainer/FractalViewport/WorldEnvironment.environment.background_color = new_color

func _on_background_color_changed(color):
    settings.bg_colour = color
    set_background(color)

func save_screenshot(screenshot_size : Vector2i):
    $FractalContainer/FractalViewport.set_size(screenshot_size)
    await RenderingServer.frame_post_draw
    img_to_save = $FractalContainer/FractalViewport.get_texture().get_image()
    $FractalContainer/FractalViewport.set_size(get_tree().root.get_viewport().size)
    _save_image()

func _on_export_btn_pressed():
    _store_camera()
    _save_settings()

func _on_import_btn_pressed():
    _load_settings()

func _on_root_resized():
    $FractalContainer/FractalViewport.size = size
