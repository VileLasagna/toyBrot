var gd_callbacks = {
   dataLoaded: null
}

function loadData() {
   var input = document.createElement('input');
   input.setAttribute('type', 'file');
   input.setAttribute('accept', '.tres');
   input.click();

   input.addEventListener('change', evt => {
      var file = evt.target.files[0];
      var reader = new FileReader();

      reader.readAsText(file);

      reader.onloadend = function(evt) {
         if (gd_callbacks.dataLoaded) {
            gd_callbacks.dataLoaded(reader.result);
         }
      }
   });
}
