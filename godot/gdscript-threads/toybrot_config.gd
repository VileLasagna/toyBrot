extends Resource
class_name TBSettings


@export_category("Window and Camera")

@export var window_size     : Vector2i = Vector2(1820,980)
@export var camera_position : Vector3 = Vector3( 0, 0, -3.8)
@export var camera_target   : Vector3 = Vector3.ZERO
@export var camera_fov      : int = 45
@export var camera_near     : float = 0.1

@export_category("Colouring")

@export var hue_factor   : float = -40.0
@export var hue_offset   : int   = 245
@export var value_factor : float = 42
@export var value_range  : float = 1.0
@export var value_clamp  : float = 0.9
@export var sat_value    : float = 0.7
@export var bg_colour    : Color = Color(0.05, 0.05, 0.05, 1.0)

@export_category("Raymarching Parameters")

@export var max_ray_steps : int = 7500
@export var collision_min_dist : float = 0.00055

@export_category("Mandelbox Parameters")

@export var fixed_radius_squared   : float = 2.2
@export var minimum_radius_squared : float = 0.8
@export var folding_limit          : float = 1.45
@export var box_scale              : float = -3.5
@export var box_iterations         : int = 30
