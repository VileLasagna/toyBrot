#!/bin/bash

NUM_RUNS=10
MAX_THREADS=24

for THREADS in $(seq 1 $MAX_THREADS)
do
    ./tbGodot.x86_64 -- --runs="${NUM_RUNS}" --threads="${THREADS}" >>  results.out
done
