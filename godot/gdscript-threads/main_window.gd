extends Node3D


@export var settings : TBSettings
@export var fracgen : RayMarcher
@export var render_surface : TextureRect

var finished : bool = false
var title_bar_updated = false
var start_time : int

var runs_per_thread_count : int = 1
var current_run_count : int = 0

@onready var window : Window = get_tree().root as Window
var clear_bg : Image

# Called when the node enters the scene tree for the first time.
func _ready():

    var arguments : Dictionary = {"threads" : 0, "runs" : 1}
    for argument in OS.get_cmdline_user_args():
        if argument.find("=") > -1:
            var key_value = argument.split("=")
            arguments[key_value[0].lstrip("--")] = int(key_value[1])
        else:
            # Options without an argument will be present in the dictionary,
            # with the value set to an empty string.
            arguments[argument.lstrip("--")] = ""

    if settings == null :
        settings = TBSettings.new()

    if fracgen == null :
        fracgen = RayMarcher.new()
        add_child(fracgen)


    if render_surface == null :
        render_surface = TextureRect.new()
        render_surface.visible = true

    if arguments["threads"] > 0:
        fracgen.num_threads = arguments["threads"]
    else:
        fracgen.num_threads = 7

    if arguments["runs"] > 0:
        runs_per_thread_count = arguments["runs"]
    else:
        runs_per_thread_count = 1

    #settings.window_size = Vector2i(500,300)
    settings.window_size = Vector2i(320,180)
    #settings.window_size = Vector2i(32,18)
    #settings.collision_min_dist = 0.000001
    settings.max_ray_steps = 1000
    settings.value_factor = 5.0
    settings.box_iterations = 10
    settings.box_scale = 3.0
    settings.folding_limit = 1.0
    settings.minimum_radius_squared = 0.5
    settings.fixed_radius_squared = 1.0

    fracgen.settings = settings
    #fracgen.num_threads = 1 #max(2, OS.get_processor_count()/3)

    $Camera.set_near(settings.camera_near)
    $Camera.set_fov(settings.camera_fov)

    fracgen.set_camera($Camera)
    fracgen.canvas_transform = get_tree().root.get_global_canvas_transform()

    window.title = "ToyBrot is generating a fractal..."
    window.set_size(settings.window_size)
    render_surface.set_size(settings.window_size)
    add_child(render_surface)
    if runs_per_thread_count > 1:
        print("num_threads	Runtime(ms)")

    _reset();


func _reset():
    clear_bg = Image.create(settings.window_size.x, settings.window_size.y, false, Image.FORMAT_RGBAF)
    clear_bg.fill(settings.bg_colour)
    fracgen.fractal_image = clear_bg
    render_surface.texture = ImageTexture.create_from_image(clear_bg)
    fracgen.initialise()
    finished = false
    start_time = Time.get_ticks_msec()

func _process( _delta ) -> void:
    if !finished:
        var new_data : bool = fracgen.compute_and_check()
        if new_data:
            render_surface.texture.update(fracgen.fractal_image)
            finished = fracgen.generation_finished
    elif runs_per_thread_count > 1:
        print(str(fracgen.num_threads,"	",Time.get_ticks_msec() - start_time))
        current_run_count += 1
        if current_run_count >= runs_per_thread_count:
            current_run_count = 0
            #assume this is a benchmark run and just bail
            get_tree().quit()
        else:
            _reset()
    elif !title_bar_updated:
        window.title = str("Generation took ", Time.get_ticks_msec() - start_time, "ms")
        title_bar_updated = true


