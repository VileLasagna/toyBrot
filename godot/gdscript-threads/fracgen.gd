extends Node
class_name RayMarcher

@export var settings : TBSettings

var num_threads : int = 10
var last_height : int = 0
var height_step : int = 10
var progressive : bool = true
var fractal_image : Image
var generation_finished : bool = false
var time_to_quit : bool = false
var round_done : bool = true

var cam_transform    : Transform3D
var canvas_transform : Transform2D
var cam_projection   : Projection

var spin = false

var rays : Array[Vector3]
var main_semaphore : Semaphore
var control_thread : Thread
var thread_semaphores : Array[Semaphore]
var thread_pool : Array[Thread]
var results : Array[bool]
var thread_is_idle : Array[bool]

var folding_limit : Vector3


func set_camera(cam : Camera3D) -> void:
    settings.camera_position = cam.get_global_position()
    cam_transform = cam.get_camera_transform()
    cam_projection = cam.get_camera_projection()
    rays = []
    rays.resize(settings.window_size.y * settings.window_size.x)
    for h in settings.window_size.y:
        for w in settings.window_size.x:
            rays[w + (h * settings.window_size.x) ] = cam.project_local_ray_normal(Vector2(w, h))

################################################################################
#
# Distance estimator functions and helpers
#
################################################################################

func _sphere_dist(point: Vector3) -> float :
    return point.length() - 2.0


func _box_dist(point : Vector3) -> float :
    # Distance estimator for a mandelbox
    #
    # Distance estimator adapted from
    # http://blog.hvidtfeldts.net/index.php/2011/11/distance-estimated-3d-fractals-vi-the-mandelbox/
    var offset : Vector3 = point
    var dr = settings.box_scale
    var z : Vector3 = point
    for i in settings.box_iterations:
        #Box fold
        z = z.clamp(-folding_limit, folding_limit) * 2 - z

        #Sphere fold
        var r_squared : float = z.length_squared()
        if r_squared < settings.minimum_radius_squared :
            # linear inner scaling
            var temp : float = settings.fixed_radius_squared / settings.minimum_radius_squared
            z *= temp
            dr *= temp
        elif r_squared < settings.fixed_radius_squared :
            # this is the actual sphere inversion
            var temp : float = settings.fixed_radius_squared/r_squared
            z *= temp
            dr *= temp

        z = z * settings.box_scale + offset
        dr = dr * absf(settings.box_scale) + 1.0

    return z.length()/absf(dr)

################################################################################
#
# Colouring
#
################################################################################

func _get_colour(steps : Vector4) -> Color:
    if steps.w >= settings.max_ray_steps:
        return settings.bg_colour

    var hue : float = fmod((steps.z * settings.hue_factor) + settings.hue_offset, 360)/360
    var val : float = settings.value_range * (1 - min(steps.w*settings.value_factor/settings.max_ray_steps,settings.value_clamp))

    #return Color(val, val, val, 1.0)
    return Color.from_hsv(hue, settings.sat_value, val)

################################################################################
#
# Ray marching functions and helpers
#
# _project_ray_normal and everything under it adapted from Godot's own code
#
################################################################################


func _project_local_ray_normal( pixel_coord : Vector2i) -> Vector3 :
    var cpos = canvas_transform * (pixel_coord as Vector2)
    var screen_he : Vector2 = cam_projection.get_viewport_half_extents()
    return Vector3(((cpos.x / settings.window_size.x) * 2.0 - 1.0) * screen_he.x, ((1.0 - (cpos.y / settings.window_size.y)) * 2.0 - 1.0) * screen_he.y, -settings.camera_near).normalized()


func _project_ray_normal(pixel_coord : Vector2i) -> Vector3:

    var ray: Vector3 = _project_local_ray_normal(pixel_coord)
    return cam_transform.basis*ray.normalized()



func _trace(pixel_coord : Vector2i) -> Vector4 :
    # This function taken from
    # http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
    var total_distance : float = 0.0
    var steps : int = 0
    var ray_direction : Vector3 = _project_ray_normal(pixel_coord)
    #var ray_direction = rays[pixel_coord.x + (pixel_coord.y * settings.window_size.x)]
    var p : Vector3 = Vector3.ZERO
    for i in settings.max_ray_steps:
        steps += i
        p = settings.camera_position + (ray_direction*total_distance)
        var distance : float = _sphere_dist(p)
        total_distance += distance
        if distance < settings.collision_min_dist:
            break
    return Vector4(p.x, p.y, p.z, steps)

func _trace_region(idx : int) -> void:
    while true:
        thread_semaphores[idx].wait()
        if time_to_quit:
            return
        var height_range : int = last_height + ( height_step if progressive else settings.window_size.y )
        for h in range(last_height, height_range, 1):
            if (h >= settings.window_size.y):
                results[idx] = true
                break
            for w in range(idx, settings.window_size.x, num_threads):
                if w == 0 :
                    fractal_image.set_pixel(w, h, Color("#FF22BBFF"))
                else:
                    fractal_image.set_pixel(w, h, _get_colour(_trace(Vector2i(w, h))))
        thread_is_idle[idx] = true

################################################################################
#
# operational section
#
################################################################################

func initialise() -> void:
    if folding_limit == null:
        folding_limit = Vector3(settings.folding_limit, settings.folding_limit, settings.folding_limit)

    if fractal_image == null:
        fractal_image = Image.create(settings.window_size.x, settings.window_size.y, false, Image.FORMAT_RGBAF)

    if thread_semaphores == null:
        thread_semaphores = []

    if results == null:
        results = []

    if thread_pool == null:
        thread_pool = []


    if thread_is_idle == null:
        thread_is_idle = []

    if main_semaphore == null:
        main_semaphore = Semaphore.new()

    if control_thread == null:
        control_thread = Thread.new()
        control_thread.start(_iterate)

    if thread_semaphores.size() != num_threads:
        thread_semaphores.resize(num_threads)
        for i in thread_semaphores.size():
            if thread_semaphores[i] == null:
                thread_semaphores[i] = Semaphore.new()

    if results.size() != num_threads:
        results.resize(num_threads)
    for i in results.size():
        results[i] = false

    if thread_pool.size() != num_threads:
        thread_pool.resize(num_threads)
        for i in thread_pool.size():
            if thread_pool[i] == null:
                thread_pool[i] = Thread.new()
                thread_pool[i].start(_trace_region.bind(i), Thread.PRIORITY_HIGH)

    if thread_is_idle.size() != num_threads:
        thread_is_idle.resize(num_threads)

    if main_semaphore == null:
        main_semaphore = Semaphore.new()

    if control_thread == null:
        control_thread = Thread.new()
        control_thread.start(_iterate)

    last_height = 0
    generation_finished = false
    time_to_quit = false
    round_done = true

func _exit_tree():
    time_to_quit = true
    main_semaphore.post()
    control_thread.wait_to_finish()



func compute_and_check() -> bool:
    if round_done:
        # main thread is idle
        if !generation_finished:
            main_semaphore.post()
        return true
    return false


func _iterate() -> void :
    while true:
        main_semaphore.wait()

        round_done = false
        if time_to_quit:
            for i in num_threads:
                thread_semaphores[i].post()
            for i in num_threads:
                thread_pool[i].wait_to_finish()
            return

        for i in num_threads:
            thread_is_idle[i] = false
            thread_semaphores[i].post()

        for idx in thread_is_idle.size():
            while not thread_is_idle[idx]:
                # block until each thread is finished
                continue

        round_done = true

        last_height += height_step
        spin = !spin
        for b in results:
            if b:
                last_height = 0
                generation_finished = true
