#ifndef TOYBROTOPENCL_HPP
#define TOYBROTOPENCL_HPP

#include <godot_cpp/classes/node.hpp>

#include <thread>
#include <mutex>
#include <condition_variable>

#include <godot_cpp/classes/image.hpp>

#include <FracGen.hpp>

class ToybrotOpenCL : public godot::Node
{
    GDCLASS(ToybrotOpenCL, Node)

public:
    ToybrotOpenCL();
    ~ToybrotOpenCL() override;
    godot::Ref<godot::Image> get_fractal();
    void generate();

protected:
    static void _bind_methods();

private:

    void _generation();


    bool exit;

    std::unique_ptr<FracGen> generator;

    std::thread fractalThread;
    std::condition_variable cv;
    std::mutex mtx;

};

#endif // TOYBROTOPENCL_HPP
