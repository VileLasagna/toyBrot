#include "toybrotopencl.hpp"
#include "godot_cpp/core/object.hpp"
#include "godot_cpp/classes/engine.hpp"

/******************************************************************************
 *
 * Default parameters
 *
 ******************************************************************************/

// Camera parameters

static constexpr const tbFPType cameraX         =  0.0;
static constexpr const tbFPType cameraY         =  0.0;
static constexpr const tbFPType cameraZ         = -3.8;
static constexpr const tbFPType targetX         =  0.0;
static constexpr const tbFPType targetY         =  0.0;
static constexpr const tbFPType targetZ         =  0.0;
static constexpr const uint32_t screenWidth     =  1820;
static constexpr const uint32_t screenHeight    =  980;
static constexpr const tbFPType fovY            =  45;
static constexpr const tbFPType near            =  0.1;

// Coulouring parameters

static constexpr const tbFPType hueFactor   = -60.0;
static constexpr const int32_t  hueOffset   =  100;
static constexpr const tbFPType valueFactor =  32;
static constexpr const tbFPType valueRange  =  1.0;
static constexpr const tbFPType valueClamp  =  0.95;
static constexpr const tbFPType satValue    =  0.7;
static constexpr const tbFPType bgRed       =  0.05;
static constexpr const tbFPType bgGreen     =  0.05;
static constexpr const tbFPType bgBlue      =  0.05;
static constexpr const tbFPType bgAlpha     =  1.0;

// Raymarching parameters

static constexpr const uint32_t  maxRaySteps      = 7500;
static constexpr const tbFPType  collisionMinDist = 0.00055;

// Mandelbox parameters

static constexpr const tbFPType  fixedRadiusSq =  2.2;
static constexpr const tbFPType  minRadiusSq   =  0.8;
static constexpr const tbFPType  foldingLimit  =  1.45;
static constexpr const tbFPType  boxScale      = -3.5;
static constexpr const uint32_t  boxIterations =  30;


ToybrotOpenCL::ToybrotOpenCL()
    :fractalThread{&ToybrotOpenCL::_generation, this}
{
    if(godot::Engine::get_singleton()->is_editor_hint())
    {
        return;
    }

    exit = false;
    CameraPtr camera = std::make_shared< Camera<tbFPType> >( Vec3<tbFPType>{cameraX, cameraY, cameraZ}
                                                 , Vec3<tbFPType>{targetX,targetY,targetZ}
                                                 , screenWidth, screenHeight, near, fovY );

    ParamPtr params = std::make_shared< Parameters<tbFPType> >( maxRaySteps, collisionMinDist, hueFactor, hueOffset
                                                     , valueFactor, valueRange, valueClamp, satValue
                                                     , bgRed, bgGreen, bgBlue, bgAlpha, fixedRadiusSq
                                                     , minRadiusSq, foldingLimit, boxScale, boxIterations  );

    generator = std::make_unique<FracGen>(false, camera, params);
}

ToybrotOpenCL::~ToybrotOpenCL()
{
    exit = true;
    if(fractalThread.joinable())
    {
        fractalThread.join();
    }
}


godot::Ref<godot::Image> ToybrotOpenCL::get_fractal()
{
    if(!generator)
    {
        return {};
    }
    godot::PackedByteArray data;
    data.resize(screenWidth * screenHeight * 4 * sizeof(tbFPType));

    memcpy(data.begin().operator->(), generator->getBuffer().get()->data(), data.size());
    return godot::Image::create_from_data(screenWidth, screenHeight, false, godot::Image::FORMAT_RGBAF, data);
}

void ToybrotOpenCL::generate()
{
    std::unique_lock<std::mutex> lock(mtx);
    cv.notify_one();
}

void ToybrotOpenCL::_bind_methods()
{

    godot::ClassDB::bind_method(godot::D_METHOD("_generation"), &ToybrotOpenCL::_generation);
    godot::ClassDB::bind_method(godot::D_METHOD("generate"), &ToybrotOpenCL::generate);
    godot::ClassDB::bind_method(godot::D_METHOD("get_fractal"), &ToybrotOpenCL::get_fractal);

    ADD_SIGNAL( godot::MethodInfo("update_available", godot::PropertyInfo(godot::Variant::OBJECT, "node")) );
    ADD_SIGNAL( godot::MethodInfo("generation_finished", godot::PropertyInfo(godot::Variant::OBJECT, "node")) );
}

void ToybrotOpenCL::_generation()
{
    std::unique_lock<std::mutex> lock(mtx);
    while(true)
    {
        // Every second we check if we're not just leaving altogether
        auto status = cv.wait_for(lock, std::chrono::seconds(1));
        if(exit || !generator || godot::Engine::get_singleton()->is_editor_hint())
        {
            return;
        }
        if(status == std::cv_status::timeout)
        {
            continue;
        }
        const bool finished = generator->Generate();
        if(finished)
        {
            call_deferred("emit_signal", "generation_finished", this);
        }
        call_deferred("emit_signal", "update_available", this);
    }
}
