extends Control

var generating : bool = false
var time : float = 0.0

@onready var threads_btn: Button = $BtnList/ThreadsBtn
@onready var cl_btn: Button = $BtnList/OpenCLBtn

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    time += delta * 4
    $WorkIndicator.rotation += 3.0 * delta
    $WorkIndicator.scale = Vector2.ONE * (1 + (sin(time) * 0.05))


func _on_generate_btn_pressed():
    get_tree().call_group("GenButtons", "set_disabled", true)
    $WorkIndicator.set_visible(true)
    generating = true

func _on_generate_threads_pressed():
    _on_generate_btn_pressed()
    $tbCppThreads.generate()

func _on_generate_opencl_pressed():
    _on_generate_btn_pressed()
    $tbOpenCL.generate()

func _on_toybrot_update_available(node):
    var img = node.get_fractal()
    $FractalTexture.set_texture(ImageTexture.create_from_image(img))
    if(generating):
       node.generate()

func _on_generation_finished(_node):
    generating = false
    get_tree().call_group("GenButtons", "set_disabled", false)
    $WorkIndicator.set_visible(false)
