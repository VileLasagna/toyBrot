#ifndef CPP_TASKS_REGISTER_TYPES_H
#define CPP_TASKS_REGISTER_TYPES_H

#include <godot_cpp/godot.hpp>


void initialize_tb_cpp_tasks_types(godot::ModuleInitializationLevel p_level);
void uninitialize_tb_cpp_tasks_types(godot::ModuleInitializationLevel p_level);

#endif // CPP_TASKS_REGISTER_TYPES_H
