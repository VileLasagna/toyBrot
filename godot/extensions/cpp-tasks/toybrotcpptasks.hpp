#ifndef TOYBROTCPPTASKS_HPP
#define TOYBROTCPPTASKS_HPP

#include <godot_cpp/classes/node.hpp>

#include <godot_cpp/classes/image.hpp>

#include "FracGen.hpp"

#ifdef TOYBROT_USE_DOUBLES
    #include <godot_cpp/variant/packed_float64_array.hpp>
    using tbArrayType = godot::PackedFloat64Array;
#else
    #include <godot_cpp/variant/packed_float32_array.hpp>
    using tbArrayType = godot::PackedFloat32Array;
#endif

class ToybrotCppTasks : public godot::Node
{
    GDCLASS(ToybrotCppTasks, Node)

public:
    ToybrotCppTasks();
    godot::Ref<godot::Image> get_fractal();
    bool generate();

protected:
    static void _bind_methods();

private:

    std::unique_ptr<FracGen> generator;

};

#endif // TOYBROTCPPTASKS_HPP
