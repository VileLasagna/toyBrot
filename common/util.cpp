#include "util.hpp"

#include <cstring>

#ifndef TB_NOSTDOUT
    #include <iostream>
#endif

#ifdef TB_GDEX
    #include <gdextension_interface.h>
    #include <godot_cpp/variant/utility_functions.hpp>
#endif


std::streambuf* toyBrot::messageBuffer::get()
{
#ifndef TB_NOSTDOUT
    return std::cout.rdbuf();
#else

    static constexpr const size_t bufferSize = 512;
    static toyBrot::messageBuffer msgBuf(bufferSize);

    return &msgBuf;
#endif
}

std::ostream toyBrot::out(toyBrot::messageBuffer::get());

toyBrot::messageBuffer::messageBuffer(size_t bufferLength)
    :std::stringbuf(std::string(bufferLength,'\0'))
{}

int toyBrot::messageBuffer::sync()
{
    size_t outLength = pptr() - pbase();
#if defined(TB_GDEX) && !defined(NDEBUG)
    /*
     * std::endl is the desired/expected "close this line and flush, please" BUT
     * godot's print() already adds a newline so we need to trim that \n
     */
    auto out = str().substr(0, outLength);
    if(!out.empty() && out.back() == '\n')
    {
        out.resize(out.size() - 1);
    }
    godot::UtilityFunctions::print(out.c_str());
#endif
    std::memset(pbase(), 0, outLength);
    pubseekpos(0);

    return 0;
}


std::streamsize toyBrot::messageBuffer::xsputn(const char* s, std::streamsize n)
{
    for(std::streamsize idx = 0; idx < n; idx++)
    {
        sputc(s[idx]);
        // flush on a newline, c-string end or overflow
        // If we're not flushing on a 0 char, we add one for safety
        if(s[idx] == 0 || s[idx] == '\n' || pptr() == epptr())
        {
            sync();
        }
    }
    return n;
}

int toyBrot::messageBuffer::overflow(int c)
{
    sync();
    return sputc(c);
}
