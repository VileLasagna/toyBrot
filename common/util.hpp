#ifndef _TOYBROT_UTIL_HPP_DEFINED_
#define _TOYBROT_UTIL_HPP_DEFINED_

#include <sstream>
#include <iostream>

namespace toyBrot
{

    class messageBuffer: public std::stringbuf
    {
        public:
            static std::streambuf* get();
        protected:
            messageBuffer(size_t bufferLength);
            int sync() override;
            std::streamsize xsputn (const char* s, std::streamsize n) override;
            int overflow (int c = EOF) override;
    };

    extern std::ostream out;

}

#endif //_TOYBROT_UTIL_HPP_DEFINED_
