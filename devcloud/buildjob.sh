#!/bin/bash

#source /opt/intel/inteloneapi/setvars.sh

rm -rf /home/u121770/repos/toyBrot/build
mkdir -p /home/u121770/repos/toyBrot/build
cd /home/u121770/repos/toyBrot/build
#    -DPNG_DIR=/home/u121770/tools/lib \

/home/u121770/tools/cmake-3.22.1-linux-x86_64/bin/cmake \
    -DCMAKE_CXX_COMPILER=dpcpp \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_FLAGS_RELEASE="-DNDEBUG -O3 -fp-model=precise -fimf-arch-consistency=true -mavx2" \
    -DISPC_MULTI_TARGET=OFF \
    -DCMAKE_INSTALL_PREFIX=/home/u121770/repos/toyBrot/redist \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    ..

/home/u121770/tools/cmake-3.22.1-linux-x86_64/bin/cmake --build . && /home/u121770/tools/cmake-3.22.1-linux-x86_64/bin/cmake --install .

#/home/u121770/tools/cmake-3.22.1-linux-x86_64/bin/cmake --build . --parallel 12 \




