#!/bin/bash

source /opt/intel/oneapi/setvars.sh > /dev/null 2>&1

echo "Running STD::Tasks" 
/home/u121770/repos/toyBrot/redist/bin/rmSTD_TASKS            -i 5 -o rmSTD_TASKS.batch.out -p
echo ""
echo ""
echo "Running TBB"
/home/u121770/repos/toyBrot/redist/bin/rmTBB                  -i 5 -o rmTBB.batch.out -p
echo ""
echo ""
echo "Running ISPC-avx2-i32x8"
/home/u121770/repos/toyBrot/redist/bin/rmISPC-avx2-i32x8      -i 5 -o rmISPC-avx2-i32x8.batch.out -p
echo ""
echo ""
echo "Running ISPC-avx512-i32x8"
/home/u121770/repos/toyBrot/redist/bin/rmISPC-avx512skx-i32x8 -i 5 -o rmISPC-avx512skx-i32x8.batch.out -p
echo ""
echo ""
echo "Running SYCL"
/home/u121770/repos/toyBrot/redist/bin/rmSYCL                 -i 5 -o rmSYCL.batch.out -p

echo ""
echo ""
echo "Running SYCL on CPU"
/home/u121770/repos/toyBrot/redist/bin/rmSYCL-cpu                 -i 5 -o rmSYCL.batch.out -p

echo ""
echo ""
echo "Running SYCL on FPGA"
/home/u121770/repos/toyBrot/redist/bin/rmSYCL-fpga                 -i 5 -o rmSYCL.batch.out -p

echo ""
echo ""
echo "Running SYCL on FPGA(Arria10)"
/home/u121770/repos/toyBrot/redist/bin/rmSYCL-fpga-arria10                 -i 5 -o rmSYCL.batch.out -p

echo ""
echo ""
echo "Running SYCL on FPGA(Starix10)"
/home/u121770/repos/toyBrot/redist/bin/rmSYCL-fpga-stratix10                 -i 5 -o rmSYCL.batch.out -p

