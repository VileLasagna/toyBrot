#include "FracGen.hpp"

#include <iostream>

#include "util.hpp"

#if defined(TB_SYCL_INTELFPGA)
    #include <sycl/ext/intel/fpga_extensions.hpp>
#endif


/******************************************************************************
 *
 * Distance estimator functions and helpers
 *
 ******************************************************************************/

inline void sphereFold(Vec3<tbFPType>& z, tbFPType& dz , const Parameters<tbFPType>& params)
{
    tbFPType r2 = z.sqMod();
    if ( r2 < params.MinRadiusSq())
    {
        // linear inner scaling
        tbFPType temp = (params.FixedRadiusSq()/params.MinRadiusSq());
        z *= temp;
        dz *= temp;
    }
    else if(r2<params.FixedRadiusSq())
    {
        // this is the actual sphere inversion
        tbFPType temp =(params.FixedRadiusSq()/r2);
        z *= temp;
        dz*= temp;
    }
}

inline void boxFold(Vec3<tbFPType>& z , const Parameters<tbFPType>& params)
{
    z = z.clamp(-params.FoldingLimit(), params.FoldingLimit()) * static_cast<tbFPType>(2) - z;
}

tbFPType boxDist(const Vec3<tbFPType>& p , const Parameters<tbFPType>& params)
{
    /**
     * Distance estimator for a mandelbox
     *
     * Distance estimator adapted from
     * https://http://blog.hvidtfeldts.net/index.php/2011/11/distance-estimated-3d-fractals-vi-the-mandelbox/
     */
    const Vec3<tbFPType>& offset = p;
    tbFPType dr = params.BoxScale();
    Vec3<tbFPType> z{p};
    for (size_t n = 0; n < params.BoxIterations(); n++)
    {
        boxFold(z, params);       // Reflect
        sphereFold(z, dr, params);    // Sphere Inversion

        z = z * params.BoxScale() + offset;  // Scale & Translate
        dr = dr * cl::sycl::fabs(params.BoxScale()) + 1.0f;

    }
    tbFPType r = z.mod();
    return r/cl::sycl::fabs(dr);
}


tbFPType bulbDist(const Vec3<tbFPType>& p)
{

    /**
     * Distance estimator for a mandelbulb
     *
     * Distance estimator adapted from
     * https://www.iquilezles.org/www/articles/mandelbulb/mandelbulb.htm
     * https://www.shadertoy.com/view/ltfSWn
     */

    Vec3<tbFPType> w = p;
    tbFPType m = w.sqMod();

    //vec4 trap = vec4(abs(w),m);
    tbFPType dz = 5.0f;


    for( int i=0; i<4; i++ )
    {
#if 1
        tbFPType m2 = m*m;
        tbFPType m4 = m2*m2;
        dz = 8.0f*sqrt(m4*m2*m)*dz + 1.0f;

        tbFPType x = w.X(); tbFPType x2 = x*x; tbFPType x4 = x2*x2;
        tbFPType y = w.Y(); tbFPType y2 = y*y; tbFPType y4 = y2*y2;
        tbFPType z = w.Z(); tbFPType z2 = z*z; tbFPType z4 = z2*z2;

        tbFPType k3 = x2 + z2;
        tbFPType k2 = 1/sqrt( k3*k3*k3*k3*k3*k3*k3 );
        tbFPType k1 = x4 + y4 + z4 - 6.0f*y2*z2 - 6.0f*x2*y2 + 2.0f*z2*x2;
        tbFPType k4 = x2 - y2 + z2;

        w.setX(p.X() +  64.0f*x*y*z*(x2-z2)*k4*(x4-6.0f*x2*z2+z4)*k1*k2);
        w.setY(p.Y() + -16.0f*y2*k3*k4*k4 + k1*k1);
        w.setZ(p.Z() +  -8.0f*y*k4*(x4*x4 - 28.0f*x4*x2*z2 + 70.0f*x4*z4 - 28.0f*x2*z2*z4 + z4*z4)*k1*k2);
#else
        dz = 8.0*pow(sqrt(m),7.0)*dz + 1.0;
        //dz = 8.0*pow(m,3.5)*dz + 1.0;

        tbFPType r = w.mod();
        tbFPType b = 8.0*acos( w.Y()/r);
        tbFPType a = 8.0*atan2( w.X(), w.Z() );
        w = p + Vec3<tbFPType>( sin(b)*sin(a), cos(b), sin(b)*cos(a) ) * pow(r,8.0);
#endif

       // trap = min( trap, vec4(abs(w),m) );

        m = w.sqMod();
        if( m > 256.0f )
            break;
    }

    return 0.25f*cl::sycl::log(m)*cl::sycl::sqrt(m)/dz;
}

tbFPType sphereDist(Vec3<tbFPType> p)
{
    tbFPType radius = 2.7f;
    return p.mod() - radius;
}

/******************************************************************************
 *
 * Coulouring functions and helpers
 *
 ******************************************************************************/

RGBA HSVtoRGB(int H, tbFPType S, tbFPType V)
{

    /**
     * adapted from
     * https://gist.github.com/kuathadianto/200148f53616cbd226d993b400214a7f
     */

    RGBA output;
    tbFPType C = S * V;
    tbFPType X = C * (1 - cl::sycl::fabs(cl::sycl::fmod(H / static_cast<tbFPType>(60),  static_cast<tbFPType>(2)) - 1));
    tbFPType m = V - C;
    tbFPType Rs, Gs, Bs;

    if(H >= 0 && H < 60)
    {
        Rs = C;
        Gs = X;
        Bs = 0;
    }
    else if(H >= 60 && H < 120)
    {
        Rs = X;
        Gs = C;
        Bs = 0;
    }
    else if(H >= 120 && H < 180)
    {
        Rs = 0;
        Gs = C;
        Bs = X;
    }
    else if(H >= 180 && H < 240)
    {
        Rs = 0;
        Gs = X;
        Bs = C;
    }
    else if(H >= 240 && H < 300)
    {
        Rs = X;
        Gs = 0;
        Bs = C;
    }
    else {
        Rs = C;
        Gs = 0;
        Bs = X;
    }

    output.setR(Rs + m);
    output.setG(Gs + m);
    output.setB(Bs + m);
    output.setA(1);

    return output;
}

RGBA getColour(const Vec4<tbFPType>& steps, const Parameters<tbFPType>& params)
{
    RGBA colour;

    Vec3<tbFPType> position(steps.X(),steps.Y(),steps.Z());

    const RGBA background(params.BgRed(), params.BgGreen(), params.BgBlue(), params.BgAlpha());

    if(steps.W() == params.MaxRaySteps())
    {
        return background;
    }

    tbFPType saturation = params.SatValue();
    tbFPType hueVal = (position.Z() * params.HueFactor()) + params.HueOffset();
    int hue = static_cast<int>( cl::sycl::trunc(cl::sycl::fmod(hueVal, static_cast<tbFPType>(360)) ) );
    hue = hue < 0 ? 360 + hue: hue;
    tbFPType value = params.ValueRange() * (static_cast<tbFPType>(1) - cl::sycl::min(steps.W()*params.ValueFactor()/params.MaxRaySteps(), params.ValueClamp()));

    colour = HSVtoRGB(hue, saturation, value);

//  Simplest colouring, based only on steps (roughly distance from camera)
    //colour = RGBA(value,value,value,1.0f);

    return colour;
}

/******************************************************************************
 *
 * Ray marching functions and helpers
 *
 ******************************************************************************/

Vec4<tbFPType> trace(const Camera<tbFPType>& cam, const Parameters<tbFPType>& params, int x, int y)
{
    /**
     * This function taken from
     * http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
     */

    tbFPType totalDistance = 0;
    unsigned int steps;

    Vec3<tbFPType> pixelPosition = cam.ScreenTopLeft() + (cam.ScreenRight()*static_cast<tbFPType>(x)) + (cam.ScreenUp() * static_cast<tbFPType>(y));

    Vec3<tbFPType> rayDir = pixelPosition -  cam.Pos();
    rayDir.normalise();

    Vec3<tbFPType> p;
    for (steps=0; steps < params.MaxRaySteps(); steps++)
    {
        p = cam.Pos() + (rayDir * totalDistance);
        //tbFPType distance = cl::sycl::min(boxDist(p),bulbDist(p));
        tbFPType distance = boxDist(p, params);
        totalDistance += distance;
        if (distance < params.CollisionMinDist()) break;
    }
    //return both the steps and the actual position in space for colouring purposes
    return Vec4<tbFPType>{p,static_cast<tbFPType>(steps)};
}

template < typename Accessor>
void traceRegion(Accessor data,
                 Camera<tbFPType> cam, Parameters<tbFPType> params,
                 /*cl::sycl::id<2> tid*/cl::sycl::nd_item<2> tid)
{
    int col   = tid.get_global_id(0);
    int row   = tid.get_global_id(1);
    int index = ((row*cam.ScreenWidth())+col);

    if (col > cam.ScreenWidth() || row > cam.ScreenHeight() || index > cam.ScreenWidth()*cam.ScreenHeight())
    {
        return;
    }

    data[index] = getColour(trace(cam, params, col, row), params);
}

/******************************************************************************
 *
 * Thread spawning section
 *
 ******************************************************************************/

void FracGen::Generate()
{
    if(outBuffer->size() != cam->ScreenWidth()*cam->ScreenHeight())
    {
        outBuffer->resize(cam->ScreenWidth()*cam->ScreenHeight());
    }
    try
    {
        Camera<tbFPType>     c (*cam);
        Parameters<tbFPType> p (*parameters);

        //This is how we'll split the workload
        cl::sycl::range<2> workgroup(16,16);
        const size_t globalWidth = cam->ScreenWidth()%workgroup.get(0) == 0? cam->ScreenWidth() : ((cam->ScreenWidth()/workgroup.get(0))+1)*workgroup.get(0);
        const size_t globalHeight = cam->ScreenHeight()%workgroup.get(1) == 0? cam->ScreenHeight() : ((cam->ScreenHeight()/workgroup.get(1))+1)*workgroup.get(1);
        cl::sycl::range<2> pixels(globalWidth,globalHeight);
        //Create the device-side buffer
        cl::sycl::buffer<RGBA,1> buff (outBuffer->data(), cam->ScreenWidth()*cam->ScreenHeight());
        q->submit([&](cl::sycl::handler& cgh)
                        {
                            auto access_v = buff.get_access<cl::sycl::access::mode::write>(cgh);
                            cgh.parallel_for<class syclmarchingkernel>
                                    (  cl::sycl::nd_range<2>(pixels, workgroup),
                                       [=] (/*cl::sycl::id<2> tid*/cl::sycl::nd_item<2> tid)
                                       { traceRegion(access_v, c, p,tid);}
                                    );
                        });
        q->wait_and_throw();
    }
    catch(cl::sycl::exception const& e)
    {
        toyBrot::out << "SYCL sync exception -> " << e.what() << std::endl;
    }
    catch(...)
    {
        toyBrot::out << " Exception caught! " << std::endl;
    }
}

FracGen::FracGen(bool benching, CameraPtr c, ParamPtr p)
    : bench{benching}
    , cam{c}
    , parameters{p}
{
    outBuffer = std::make_shared< colourVec >(cam->ScreenWidth()*cam->ScreenHeight());

    static bool once = false;

    #if defined(TB_SYCL_INTELFPGA)
        cl::sycl::ext::intel::fpga_selector device_selector;
    #elif defined(TB_SYCL_CPU)
        cl::sycl::host_selector device_selector;
    #else
        cl::sycl::default_selector device_selector;
    #endif

    cl::sycl::async_handler sycl_err_handler =  [] (cl::sycl::exception_list exceptions)
                                            {
                                                for (std::exception_ptr const& e : exceptions)
                                                {
                                                    try
                                                    {
                                                        std::rethrow_exception(e);
                                                    }
                                                    catch(cl::sycl::exception const& e)
                                                    {
                                                        toyBrot::out << "SYCL async exception -> " << e.what() << std::endl;
                                                    }
                                                }
                                            };

    q = std::make_unique<cl::sycl::queue>(device_selector, sycl_err_handler);

    if(!once || !bench )
    {
        toyBrot::out << "Running on "
                  << q->get_device().get_info<cl::sycl::info::device::name>()
                  << std::endl ;
        once = true;
    }
}

FracGen::~FracGen()
{}



