#!/bin/bash

#Cause the script to exit if any commands fail
# set -e
# set -o pipefail
shopt -s nullglob

rm -rf ./toybrot.out.d/*

RUN_GPU=0
RUN_CPU=1
RUN_ISPC=0

WIDTH=1920
HEIGHT=1200

CPU=( "rmSTD_" "rmOpenMP" "rmISPC" "rmTBB")
NV=("rmCUDA")
AMD=("rmHIP" "rmSYCL")
GPU_OTHER=("rmVulkan" "rmOpenCL" "FracGen.")
ISPC=("ISPC-sse" "ISPC-avx" "ISPC-TBB-sse" "ISPC-TBB-avx")

ITERATIONS="10"

COMPILERS=$(ls -I "toybrot.*" -I "bench.sh" /toyBrot/)

IGNORE=("toybrot." "THREADS-ptr")

if [ $RUN_GPU -eq 0 ]; then
    for IMP in ${NV[@]}; do
        IGNORE+=(${IMP})
    done

    for IMP in ${AMD[@]}; do
        IGNORE+=(${IMP})
    done


    for IMP in ${GPU_OTHER[@]}; do
        IGNORE+=(${IMP})
    done
fi

if [ $RUN_CPU -eq 0 ]; then
    for IMP in ${CPU[@]}; do
        IGNORE+=(${IMP})
    done
fi

if [ $RUN_ISPC -eq 0 ]; then
    for IMP in ${ISPC[@]}; do
        IGNORE+=(${IMP})
    done
fi

#echo "IGNORE=${IGNORE[@]}"

for DIR in ${COMPILERS[@]}; do
    
    mkdir -p /toyBrot/toybrot.out.d/$DIR
    mkdir -p /toyBrot/toybrot.out.d/$DIR-db
    
    cd /toyBrot/$DIR/bin/
    # echo
    # echo
    # echo $DIR
    # echo

    ALLFLOATS=(/toyBrot/$DIR/bin/* )
    ALLDOUBLES=(/toyBrot/$DIR/bin/*-db)
    FLOATS=()
    DOUBLES=()

    for IMP in ${ALLFLOATS[@]}; do
        DISCARD=0
        # clean list of ignored entries
        for IGN in ${IGNORE[@]}; do
            if [[ $IMP == *"$IGN"* ]]; then
                DISCARD=1
                break;
            fi 
             if [[ $IMP == *"-db" ]]; then
                DISCARD=1
                break;
            fi 
        done;
        if [ $DISCARD -ne 0 ]; then
            continue;
        else
            FLOATS+=("$IMP")
        fi

    done

    #double versions
    for IMP in ${ALLDOUBLES[@]}; do :
        DISCARD=0
        # clean list of ignored entries
        for IGN in ${IGNORE[@]}; do
            if [[ $IMP == *"$IGN"* ]]; then
                DISCARD=1
                break;
            fi 
        done;
        if [ $DISCARD -ne 0 ]; then
            continue;
        else
            DOUBLES+=("$IMP")
        fi
    done

    echo "Benchmarking single-precision for $DIR"
    for IMP in ${FLOATS[@]}; do
        $IMP -d $WIDTH $HEIGHT -i $ITERATIONS -o toybrot.out.d/$(basename $IMP).out -p
    done

    echo "Benchmarking double-precision for $DIR"
    for IMP in ${DOUBLES[@]}; do
        $IMP -d $WIDTH $HEIGHT -i $ITERATIONS -o toybrot.out.d/$(basename $IMP).out -p
    done

    
done