# -*- mode: dockerfile -*-
# # vi: set ft=dockerfile :


FROM registry.gitlab.com/vilelasagna/opensuse-cuda-docker:11.2.2-devel-leap15.2

ARG ROCMVER=4.3

RUN rpm --import https://repo.radeon.com/rocm/rocm.gpg.key
RUN zypper -n ar -ef -p 50 https://repo.radeon.com/rocm/zyp/${ROCMVER}/ rocm
# ROCm depends on a perl-URI-Encode package that only exists here
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/languages:/perl/openSUSE_Leap_15.2/ devel:languages:perl
# Need this one for CMake 3.21+ since I'm relying on the HIP integration from that version
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/tools:/building/openSUSE_Leap_15.2 devel:tools:building
# I've already updated OpenCL headers so that it's CL/opencl.hpp but, this is a somewhat new change
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/science/openSUSE_Leap_15.2 science
# For ISPC
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/libraries:/c_c++/openSUSE_Leap_15.2 devel:libraries:c_c++
# For newer TBB with the CMake package files
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/tools:/compiler/openSUSE_Leap_15.2 devel:tools:compiler
# newer Vulkan headers
RUN zypper ar -p 50 -ef https://download.opensuse.org/repositories/X11:/Wayland/openSUSE_Leap_15.2 X11:Wayland
# Intel GPU OpenCL SDK and tools
#RUN zypper ar -p 50 -ef https://copr.fedorainfracloud.org/coprs/jdanecki/intel-opencl/repo/opensuse-leap-15.2/jdanecki-intel-opencl-opensuse-leap-15.2.repo


RUN zypper --gpg-auto-import-keys --no-gpg-checks ref \
&& zypper -n --no-gpg-checks in \
sudo \
gdb \
git \
wget \
unzip \
nasm \
nano \
vim \
zlib-devel \
cmake \
ninja \
clang \
clang-devel \
libc++-devel \
lldb \
libomp-devel \
gcc10 \
gcc10-c++ \
git \
libpng16-devel \
giflib-devel \
patch \
perl \
hip-base \
rocm-smi \
rocm-utils \
rocm-opencl-devel \
rocm-dev \
hip-rocclr \
tbb \
tbb-devel \
vulkan-headers \
vulkan-devel \
glslang-devel \
opencl-headers \
opencl-cpp-headers \
libOpenCL1 \
clinfo \
ispc \
#intel-opencl \
libboost_fiber1_76_0-devel \
libboost_context1_76_0-devel \
cuda-cudart-dev-10-1 \
cuda-nvcc-10-1 \
cuda-curand-dev-10-1 \
&& rm -rf /var/cache/zypp/*

RUN ln -s /usr/local/cuda-10.1/targets/x86_64-linux/include /usr/local/cuda-10.1/include 

RUN cd / && \
git clone --recurse-submodules -bv0.9.1 https://github.com/illuhad/hipSYCL.git && \
mkdir /hipSYCL/build && \
cd /hipSYCL/build && \
cmake \
    -G Ninja \
    -DCMAKE_INSTALL_PREFIX=/opt/hipSYCL \
    -DCMAKE_C_COMPILER=gcc-10 \
    -DCMAKE_CXX_COMPILER=g++-10 \
    -DDEFAULT_PLATFORM=rocm \
    -DLLVM_ROOT=/opt/rocm/llvm .. && \
cmake --build . && cmake --install . && \
cd / && rm -rf /hipSYCL

RUN echo export HIPSYCL_PLATFORM=hip >> /etc/profile.d/hipsycl.sh

RUN cd / \
&& git clone https://github.com/google/clspv.git \
&& cd /clspv && python3 utils/fetch_sources.py \
&& mkdir build && cd build \
&& cmake -DCMAKE_BUILD_TYPE=Release -GNinja -DCMAKE_INSTALL_PREFIX=/opt/clspv /clspv \
&& cmake --build . && cmake --install . && cd / && rm -rf ./clspv

RUN echo  "export PATH=\"${PATH:+$PATH:}/opt/clspv/bin\"" >> /etc/profile.d/clspv.sh

RUN useradd -ms /bin/bash developer

ADD ./aocc-compiler-3.1.0-1.x86_64.rpm /tmp/aocc-compiler-3.1.0-1.x86_64.rpm
RUN rpm -ivh --nodeps /tmp/aocc-compiler-3.1.0-1.x86_64.rpm && rm /tmp/aocc-compiler-3.1.0-1.x86_64.rpm

# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    echo "developer:x:${uid}:${gid}:developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

RUN chmod 777 -R /home/developer

WORKDIR /home/developer
ENV HOME /home/developer

USER developer
CMD bash
