#!/bin/bash

#Cause the script to exit if any commands fail
set -e
set -o pipefail

PRECMD="rm -rf /toyBrot/docker/build && rm -rf /toyBrot/docker/redist && mkdir /toyBrot/docker/build"

MARCH=znver1

COMPILER_NAMES=(  "CLANG"                   "GCC10"                      "AOCC"                                     "ROCMCLANG"                   )
C_COMPILERS=(     "/usr/bin/clang"          "/usr/bin/gcc-10"            "/opt/AMD/aocc-compiler-3.1.0/bin/clang"   "/opt/rocm/llvm/bin/clang"    )
CXX_COMPILERS=(   "/usr/bin/clang++"        "/usr/bin/g++-10"            "/opt/AMD/aocc-compiler-3.1.0/bin/clang++" "/opt/rocm/llvm/bin/clang++"  )
CUDA_CLANG_FLAG=( "ON"                      "OFF"                        "OFF"                                      "OFF"                         )
EXTRA_CMAKE=(     ""                        ""                           "-DDISABLE_HIP=ON"                         "-DDISABLE_HIP=ON"            )
REL_FLAGS=(       "\"-O3 -march=${MARCH}\"" "\"-Ofast -march=${MARCH}\"" "\"-O3 -march=${MARCH}\""                  "\"-O3 -march=${MARCH}\""  )

POSTCMD=":"

ROCMVER="4.3"
   
DOCKER_CMD="docker"

BUILD_TYPE="Release"

INSTALL_DIR="/toyBrot/docker/redist"
DOCKER_TI="-ti"
SQUASH_IMAGE=""
DOCKER_IMG="toybrot"
DOCKER_TAG="rocm${ROCMVER}"
DOCKERFILE="Dockerfile"

BUILD_ENV="toybrot-build:${ROCMVER}"

for IDX in ${!COMPILER_NAMES[@]} ; do

    $DOCKER_CMD run --rm $DOCKER_TI \
        --user $UID \
        -v $(pwd)/../:/toyBrot \
        -w /toyBrot \
        ${BUILD_ENV} \
        bash -c " if [ $IDX -eq  0 ] ; then $(echo "$PRECMD"); fi \
        && mkdir -p docker/build/${COMPILER_NAMES[${IDX}]} \
        && cd docker/build/${COMPILER_NAMES[${IDX}]} \
        && cmake \
            -GNinja \
            -DCMAKE_HIP_ARCHITECTURES=gfx906 gfx908 \
            -DHIPSYCL_GPU_ARCH=gfx906 \
            -DHIPSYCL_TARGETS=\"hip:gfx906\" \
            -DCUDA_CLANG_DIR=/usr/local/cuda-10.1 \
            -DNVCC_CXX_INCLUDES_DIR="" \
            -DCMAKE_VERBOSE_MAKEFILE=ON \
            -DZLIB_LIBRARY=/lib64/libz.so.1 \
            -DISPC_MULTI_TARGET=ON \
            -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}/${COMPILER_NAMES[${IDX}]} \
            -DCMAKE_C_COMPILER=${C_COMPILERS[${IDX}]} \
            -DCMAKE_CXX_COMPILER=${CXX_COMPILERS[${IDX}]} \
            -DCMAKE_CXX_FLAGS_RELEASE=${REL_FLAGS[${IDX}]} \
            -DBUILD_CUDA_CLANG=${CUDA_CLANG_FLAG[${IDX}]} \
            -DOpenGL_GL_PREFERENCE=GLVND \
            -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
            ${EXTRA_CMAKE[${IDX}]} \
            /toyBrot \
        && cmake --build . \
        && cmake --install .\
        && $POSTCMD "
   
done

#$DOCKER_CMD build --no-cache --build-arg "build=${BUILD_ENV}" --build-arg "ROCMVER=${ROCMVER}" -f ./${DOCKERFILE} -t ${DOCKER_IMG}:${DOCKER_TAG} .
$DOCKER_CMD build --build-arg "build=${BUILD_ENV}" --build-arg "ROCMVER=${ROCMVER}" -f ./${DOCKERFILE} -t ${DOCKER_IMG}:${DOCKER_TAG} .
