#!/bin/bash

#Cause the script to exit if any commands fail
set -e
set -o pipefail

CUDA_VERSIONS=("11.0.3" "11.1.1" "11.2.2" "11.3.1" "11.4.3" "11.5.2" "11.6.2" "11.7.1" "11.8.0" "12.0.1" "12.1.1" "12.2.2" "12.3.1")

build_images() {
    for VERSION in "${CUDA_VERSIONS[@]}";  do
        echo "CUDA_VERSION = ${VERSION}"
        mkdir empty
        podman build --build-arg CUDA_VERSION=${VERSION} -f Dockerfile.cudatest -t toybrot-cuda:${VERSION} ./empty
        rmdir empty
    done
}

run_tests() {
    mkdir cudatestresults
    for VERSION in "${CUDA_VERSIONS[@]}";  do
        echo "CUDA_VERSION = ${VERSION}"

        podman run --rm -ti --device nvidia.com/gpu=all --security-opt=label=disable -v $(pwd)/cudatestresults:/opt/toybrot/bin/toybrot.out.d toybrot-cuda:${VERSION} ./rmCUDA-nvcc  -i 20 -o ${VERSION}-nvcc.out
        podman run --rm -ti --device nvidia.com/gpu=all --security-opt=label=disable -v $(pwd)/cudatestresults:/opt/toybrot/bin/toybrot.out.d toybrot-cuda:${VERSION} ./rmCUDA-clang -i 20 -o ${VERSION}-clang.out
    done
}

while getopts "hbr" opt; do
  case "$opt" in
    b)
        build_images
        exit 0
        ;;
    r)
        run_tests
        exit 0
        ;;
    \?)
       echo "Invalid parameter"
       echo "$0 -h for more help"
       exit 1
       ;;
  esac
done

# Display help if no option selected
display_help
exit 0
