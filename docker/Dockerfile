# -*- mode: dockerfile -*-
# # vi: set ft=dockerfile :

ARG build

FROM ${build} as build-img

FROM registry.gitlab.com/vilelasagna/opensuse-cuda-docker:11.2.2-runtime-leap15.2

ARG ROCMVER

RUN rpm --import https://repo.radeon.com/rocm/rocm.gpg.key
RUN zypper -n ar -ef -p 50 https://repo.radeon.com/rocm/zyp/${ROCMVER}/ rocm
# ROCm depends on a perl-URI-Encode package that only exists here
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/languages:/perl/openSUSE_Leap_15.2/ devel:languages:perl
# Need this one for CMake 3.21+ since I'm relying on the HIP integration from that version
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/tools:/building/openSUSE_Leap_15.2 devel:tools:building
# I've already updated OpenCL headers so that it's CL/opencl.hpp but, this is a somewhat new change
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/science/openSUSE_Leap_15.2 science
# For ISPC
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/libraries:/c_c++/openSUSE_Leap_15.2 devel:libraries:c_c++
# For newer TBB with the CMake package files
RUN zypper -n ar -ef -p 50 https://download.opensuse.org/repositories/devel:/tools:/compiler/openSUSE_Leap_15.2 devel:tools:compiler
# newer Vulkan headers
RUN zypper ar -p 50 -ef https://download.opensuse.org/repositories/X11:/Wayland/openSUSE_Leap_15.2 X11:Wayland
# Intel GPU OpenCL SDK and tools
#RUN zypper ar -p 50 -ef https://copr.fedorainfracloud.org/coprs/jdanecki/intel-opencl/repo/opensuse-leap-15.2/jdanecki-intel-opencl-opensuse-leap-15.2.repo


RUN zypper --gpg-auto-import-keys --no-gpg-checks ref \
&& zypper -n --no-gpg-checks in \
nano \
vim \
zlib \
libnuma1 \
lsb \
libomp-devel \
libgomp1 \
libpng16-16 \
libgif7 \
perl \
rocm-smi \
rocm-utils \
rocm-opencl \
#pocl \
hip-rocclr \
tbb \
vulkan \
glslang-devel \
libvulkan1 \
vulkan-validationlayers \
vulkan-tools \
libOpenCL1 \
clinfo \
#intel-opencl \
libboost_fiber1_76_0 \
libboost_context1_76_0 \
cuda-cudart-10-1 \
#cuda-nvcc-10-1 \
cuda-curand-10-1 \
libquadmath0 \
libxml2-tools \
gnuplot \
&& rm -rf /var/cache/zypp/*

# #Intel OpenCL CPU Runtime
# RUN mkdir /intelopencl && cd /intelopencl \
# && curl -O https://registrationcenter-download.intel.com/akdlm/irc_nas/15532/l_opencl_p_18.1.0.015.tgz \
# && tar -xf l_opencl_p_18.1.0.015.tgz \
# && echo "ACCEPT_EULA=accept"                                     >> /intelopencl/install.options \
# && echo "CONTINUE_WITH_OPTIONAL_ERROR=yes"                       >> /intelopencl/install.options \
# && echo "PSET_INSTALL_DIR=/opt/intel"                            >> /intelopencl/install.options \
# && echo "CONTINUE_WITH_INSTALLDIR_OVERWRITE=yes"                 >> /intelopencl/install.options \
# && echo "PSET_MODE=install"                                      >> /intelopencl/install.options \
# && echo "INTEL_SW_IMPROVEMENT_PROGRAM_CONSENT=no"                >> /intelopencl/install.options \
# && echo "COMPONENTS=;intel-openclrt__x86_64;intel-openclrt-pset" >> /intelopencl/install.options \
# && cd /intelopencl/l_opencl_p_18.1.0.015/ && ./install.sh -s /intelopencl/install.options \
# && cd / && rm -rf /intelopencl

# RUN ln -s /usr/share/OpenCL/vendors/pocl.icd /etc/OpenCL/vendors/pocl.icd && ln -s /opt/intel/opencl_compilers_and_libraries_18.1.0.015/linux/etc/intel64.icd /etc/OpenCL/vendors/intel.icd

RUN useradd -ms /bin/bash tb

# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    echo "tb:x:${uid}:${gid}:tb,,,:/home/tb:/bin/bash" >> /etc/passwd && \
    echo "tb:x:${uid}:" >> /etc/group && \
    chown ${uid}:${gid} -R /home/tb

RUN chmod 777 -R /home/tb

COPY --from=build-img /opt/hipSYCL /opt/hipSYCL
RUN echo export HIPSYCL_PLATFORM=hip >> /etc/profile.d/hipsycl.sh

COPY --from=build-img /opt/clspv /opt/clspv
RUN echo  "export PATH=\"${PATH:+$PATH:}/opt/clspv/bin\"" >> /etc/profile.d/clspv.sh


ADD --chown=tb ./redist /toyBrot
ADD --chown=tb ./bench.sh /toyBrot/bench.sh


RUN export COMPILERS=$(ls /toyBrot/) \
    && mkdir -p /toyBrot/toybrot.conf.d \
    && for COMPILER in ${COMPILERS} ; do \
        ln -s /toyBrot/toybrot.conf.d /toyBrot/$COMPILER/bin/toybrot.conf.d; \
        done \
    && for COMPILER in ${COMPILERS}  ; do \
        mkdir -p /toyBrot/toybrot.out.d/$COMPILER; \
        ln -s /toyBrot/toybrot.out.d/$COMPILER /toyBrot/$COMPILER/bin/toybrot.out.d; \
        done \
    && chown -R tb:tb /toyBrot

WORKDIR /toyBrot
ENV HOME /home/developer
ENV LD_LIBRARY_PATH /opt/rocm/hip/lib;/opt/rocm/opencl/lib

USER tb
CMD bash
